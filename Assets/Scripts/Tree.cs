using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tree : MonoBehaviour
{
    public delegate void FireHealthDelegate(float health);
    public static event FireHealthDelegate Health;

    private Rigidbody treeBody;

    private void Start()
    {
        /*
         * �������� ��������� Rigitbody
         * � �������� isKinematic = true
         * ���-�� ������ ������ ��������
         */
        treeBody = GetComponent<Rigidbody>();
        treeBody.isKinematic = true;
    }

    public void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.CompareTag("Fire"))
        {
            Health(0.1f);
            Destroy(this.gameObject);
        }
        if (collision.gameObject.CompareTag("Player"))
        {
            treeBody.isKinematic = false;
        }
    }
}
