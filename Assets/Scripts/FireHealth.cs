using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FireHealth : MonoBehaviour
{
    [SerializeField] private GameObject[] arrFireWood;
    [SerializeField] private ParticleSystem fireParticle;

    public delegate void HealthBar(float healthBar);
    public static event HealthBar UpdateHelth;

    private float demage = 0.05f;
    private float timeLeft; //������� ��������
    private float callingTheMethod = 0.7f;

    private float bonfireHealth = 1f;//�������� ������
    private float fillInYourHealth;

    private void Start()
    {
        fillInYourHealth = bonfireHealth;
        //�������� �� �������
        Tree.Health += HealingFire;
    }
    private void Update()
    {
        timeLeft -= Time.deltaTime;
        if (timeLeft < 0)
        {
            fillInYourHealth -= demage;
            timeLeft = Random.Range(2, 6);
        }

        if(fillInYourHealth <= callingTheMethod)
        {
            FireWoodEnabled(); 
        }

        if(bonfireHealth != fillInYourHealth)
        {
            UpdateHelthMetod();
        }
    }

    public void UpdateHelthMetod()
    {
        bonfireHealth = fillInYourHealth;
        UpdateHelth(bonfireHealth);
    }
    private void HealingFire(float healt)
    {
        fillInYourHealth += healt;
    }

    private void FireWoodEnabled()
    {
        if(callingTheMethod == 0.7f)
        {
            arrFireWood[0].gameObject.SetActive(false);
            arrFireWood[1].gameObject.SetActive(false);
            callingTheMethod = 0.4f;
            return;
        }
        if(callingTheMethod == 0.4f)
        {
            arrFireWood[2].gameObject.SetActive(false);
            arrFireWood[3].gameObject.SetActive(false);
        }
    }
}
