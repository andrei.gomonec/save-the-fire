using UnityEngine;
/*
 * - ������� ��������� ������ � ������������ ����� ����
 * - ��������� ����������-�������� ����� 
 */
public class Player : MonoBehaviour
{
    [SerializeField] private Joystick joystick;
    [SerializeField] ParticleSystem trace;
    private float horizontal;
    private float vertical;
    private Vector3 moveInput;
    private Vector3 moveVelocity;
    private float speed = 1.5f;
    private Rigidbody rbPlayer;
    private Animator animatorPlayer;

    private void Start()
    {
        rbPlayer = GetComponent<Rigidbody>();
        animatorPlayer = GetComponent<Animator>();
    }

    private void Update()
    {
        if (joystick.Horizontal != 0 && joystick.Vertical != 0)
        {
            animatorPlayer.Play("Walk");
            horizontal = joystick.Horizontal;
            vertical = joystick.Vertical;
            moveInput = new Vector3(horizontal, 0f, vertical);
            moveVelocity = moveInput * speed;
        }
        else 
        {
            animatorPlayer.Play("Idle");
            moveVelocity = new Vector3(0, 0, 0); 
        }

        if (Input.GetKeyDown("w"))
        {
            Boost();
        }
    }

    private void FixedUpdate()
    {
        rbPlayer.MovePosition(rbPlayer.position + moveVelocity * Time.fixedDeltaTime);
        //������� ���������
        transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(moveInput),
                             Time.deltaTime * 720);
    }

    public void Boost()
    {
        trace.Play();
        rbPlayer.AddForce(moveVelocity * 250);
    }
}
