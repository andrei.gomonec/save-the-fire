using System;
using UnityEngine;
using UnityEngine.UI;

public class HelthBar : MonoBehaviour
{
    //[SerializeField] private Text healthText;
    //[SerializeField] private Animation textAnimation;
    private Image healthLevel;

    private void Start()
    {
        FireHealth.UpdateHelth += UpdatingTheHealthBar;
        healthLevel = GetComponent<Image>();
    }

    private void UpdatingTheHealthBar(float health)
    {
        if(health > 1)
        {
            health = 1;
        }
        healthLevel.fillAmount = health;
        //healthText.text = Convert.ToString(health);
        //textAnimation.Play("textMove");
    }
}
