using UnityEngine;

public class AnimationLight : MonoBehaviour
{
    [SerializeField] AnimationCurve lightIntensity;

    private float currentTime;
    private float totalTime;
    private new Light light;

    private void Start()
    {
        light = GetComponent<Light>();
        totalTime = lightIntensity.keys[lightIntensity.keys.Length - 1].time;
    }

    private void Update()
    {
        light.intensity = lightIntensity.Evaluate(currentTime);
        currentTime += Time.deltaTime;

        if(currentTime >= totalTime)
        {
            currentTime = 0;
        }
    }
}
